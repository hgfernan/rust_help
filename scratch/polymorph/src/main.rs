struct Sedan;

impl Sedan {
    fn drive(&self) {
        println!("Sedan is driving");
    }
}

fn road_trip(vehicle : &Sedan) {
    vehicle.drive();
}

fn main() {
    let car = Sedan;
    car.drive();
    road_trip(&car);
}
